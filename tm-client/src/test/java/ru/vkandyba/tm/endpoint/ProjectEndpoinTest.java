package ru.vkandyba.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vkandyba.tm.api.service.ServiceLocator;
import ru.vkandyba.tm.component.Bootstrap;
import ru.vkandyba.tm.marker.SoapCategory;

import java.util.List;

public class ProjectEndpoinTest {
//
//    @NotNull
//    Session session = new Session();
//
//    @NotNull
//    ServiceLocator serviceLocator = new Bootstrap();
//
//    @NotNull
//    String login = "test";
//
//    @NotNull
//    String password = "test";
//
//    @NotNull
//    String projectId = "123456790";
//
//    @NotNull
//    String projectName = "test";
//
//    @NotNull
//    Integer projectIndex = 0;
//
//    @NotNull
//    String userId = "1";
//
//    @NotNull
//    Status status = Status.COMPLETED;
//
//    @Before
//    public void before(){
//        session = serviceLocator.getSessionEndpoint().openSession(login, password);
//        final Project project = new Project();
//        project.setId(projectId);
//        project.setUserId(userId);
//        project.setName(projectName);
//        serviceLocator.getProjectEndpoint().addProject(session, project);
//    }
//
//    @After
//    public void after(){
//        serviceLocator.getProjectEndpoint().clearProjects(session);
//        serviceLocator.getSessionEndpoint().closeSession(session);
//    }
//
//    @Test
//    @Category(SoapCategory.class)
//    public void findAll() {
//        List<Project> projects = serviceLocator.getProjectEndpoint().findAllProjects(session);
//        Assert.assertEquals(1, projects.size());
//    }
//
//    @Test
//    @Category(SoapCategory.class)
//    public void startById() {
//        final Project project = serviceLocator.getProjectEndpoint().startProjectById(session, projectId);
//        Assert.assertNotNull(project);
//    }
//
//    @Test
//    @Category(SoapCategory.class)
//    public void startByIndex() {
//        final Project project = serviceLocator.getProjectEndpoint().startProjectByIndex(session, projectIndex);
//        Assert.assertNotNull(project);
//    }
//
//    @Test
//    @Category(SoapCategory.class)
//    public void startByName() {
//        final Project project = serviceLocator.getProjectEndpoint().startProjectByName(session, projectName);
//        Assert.assertNotNull(project);
//    }
//
//    @Test
//    @Category(SoapCategory.class)
//    public void finishById() {
//        final Project project = serviceLocator.getProjectEndpoint().finishProjectById(session, projectId);
//        Assert.assertNotNull(project);
//    }
//
//    @Test
//    @Category(SoapCategory.class)
//    public void finishByIndex() {
//        final Project project = serviceLocator.getProjectEndpoint().finishProjectByIndex(session, projectIndex);
//        Assert.assertNotNull(project);
//    }
//
//    @Test
//    @Category(SoapCategory.class)
//    public void finishByName() {
//        final Project project = serviceLocator.getProjectEndpoint().finishProjectByName(session, projectName);
//        Assert.assertNotNull(project);
//    }
//
//    @Test
//    @Category(SoapCategory.class)
//    public void changeStatusById() {
//        final Project project = serviceLocator.getProjectEndpoint().changeProjectStatusById(session, projectId, status);
//        Assert.assertNotNull(project);
//    }
//
//    @Test
//    @Category(SoapCategory.class)
//    public void changeStatusByIndex() {
//        final Project project = serviceLocator.getProjectEndpoint().changeProjectStatusByIndex(session, projectIndex, status);
//        Assert.assertNotNull(project);
//    }
//
//    @Test
//    @Category(SoapCategory.class)
//    public void changeStatusByName() {
//        final Project project = serviceLocator.getProjectEndpoint().changeProjectStatusByName(session, projectName, status);
//        Assert.assertNotNull(project);
//    }
//
//    @Test
//    @Category(SoapCategory.class)
//    public void removeById() {
//        final Project project = serviceLocator.getProjectEndpoint().removeProjectById(session, projectId);
//        Assert.assertNotNull(project);
//    }
//
//    @Test
//    @Category(SoapCategory.class)
//    public void removeByIndex() {
//        final Project project = serviceLocator.getProjectEndpoint().removeProjectByIndex(session, projectIndex);
//        Assert.assertNotNull(project);
//    }

}
