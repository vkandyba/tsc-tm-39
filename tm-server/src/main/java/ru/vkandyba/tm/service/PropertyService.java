package ru.vkandyba.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private final String FILE_NAME = "config.properties";

    @NotNull
    private final String APPLICATION_VERSION = "application.version";

    @NotNull
    private final String APPLICATION_VERSION_DEFAULT = "";

    @NotNull
    private final String DEVELOPER_NAME = "developer.name";

    @NotNull
    private final String DEVELOPER_NAME_DEFAULT = "";

    @NotNull
    private final String DEVELOPER_EMAIL = "developer.email";

    @NotNull
    private final String DEVELOPER_EMAIL_DEFAULT = "";

    @NotNull
    private final String HASH_SECRET = "hash.secret";

    @NotNull
    private final String HASH_SECRET_DEFAULT = "";

    @NotNull
    private final String HASH_ITERATION = "hash.iteration";

    @NotNull
    private final String HASH_ITERATION_DEFAULT = "1";

    @NotNull
    private final String SERVER_HOST = "server.host";

    @NotNull
    private final String SERVER_HOST_DEFAULT = "localhost";

    @NotNull
    private final String SERVER_PORT = "server.port";

    @NotNull
    private final String SERVER_PORT_DEFAULT = "8080";

    @NotNull
    private final String SIGN_SECRET = "sign.secret";

    @NotNull
    private final String SIGN_SECRET_DEFAULT = "";

    @NotNull
    private final String SIGN_ITERATION = "sign.iteration";

    @NotNull
    private final String SIGN_ITERATION_DEFAULT = "1";

    @NotNull
    private final String JDBC_USER = "jdbc.user";

    @NotNull
    private final String JDBC_USER_DEFAULT = "postgres";

    @NotNull
    private final String JDBC_PASSWORD = "jdbc.password";

    @NotNull
    private final String JDBC_PASSWORD_DEFAULT = "postgres";

    @NotNull
    private final String JDBC_URL = "jdbc.url";

    @NotNull
    private final String JDBC_URL_DEFAULT = "jdbc:postgresql://localhost/task-manager";

    @NotNull
    private final String JDBC_DRIVER = "jdbc.driver";

    @NotNull
    private final String JDBC_DRIVER_DEFAULT = "org.postgresql.Driver";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService(){
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if(inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    private String getValue(@NotNull final String name, @NotNull final String defaultName){
        @Nullable final String systemProperty = System.getProperty(name);
        if(systemProperty != null) return systemProperty;
        @Nullable final String envProperty = System.getenv(name);
        if(envProperty != null) return envProperty;
        return properties.getProperty(name, defaultName);
    }

    @NotNull
    @Override
    public String getDeveloperName() {
        return Manifests.read("developer");
    }

    @NotNull
    @Override
    public String getDeveloperEmail() {
        return Manifests.read("email");
    }

    @NotNull
    @Override
    public String getVersion() {
        return Manifests.read("build");
    }

    @NotNull
    @Override
    public String getHashSecret() {
        return getValue(HASH_SECRET, HASH_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public String getHashIteration() {
        return getValue(HASH_ITERATION, HASH_ITERATION_DEFAULT);
    }

    @Override
    public @NotNull String getServerHost() {
        return getValue(SERVER_HOST, SERVER_HOST_DEFAULT);
    }

    @Override
    public @NotNull String getServerPort() {
        return getValue(SERVER_PORT, SERVER_PORT_DEFAULT);
    }

    @Override
    public @NotNull String getSignSecret() {
        return getValue(SIGN_SECRET, SIGN_SECRET_DEFAULT);
    }

    @Override
    public @NotNull String getSignIteration() {
        return getValue(SIGN_ITERATION, SIGN_ITERATION_DEFAULT);
    }

    @Override
    public @NotNull String getJdbcUser() {
        return getValue(JDBC_USER, JDBC_USER_DEFAULT);
    }

    @Override
    public @NotNull String getJdbcPassword() {
        return getValue(JDBC_PASSWORD, JDBC_PASSWORD_DEFAULT);
    }

    @Override
    public @NotNull String getJdbcUrl() {
        return getValue(JDBC_URL, JDBC_URL_DEFAULT);
    }

    @Override
    public @NotNull String getJdbcDriver() {
        return getValue(JDBC_DRIVER, JDBC_DRIVER_DEFAULT);
    }
}
