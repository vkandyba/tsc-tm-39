package ru.vkandyba.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.repository.IRepository;
import ru.vkandyba.tm.api.repository.ITaskRepository;
import ru.vkandyba.tm.api.repository.IUserRepository;
import ru.vkandyba.tm.api.service.IConnectionService;
import ru.vkandyba.tm.api.service.IPropertyService;
import ru.vkandyba.tm.api.service.IUserService;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.exception.empty.*;
import ru.vkandyba.tm.exception.entity.UserNotFoundException;
import ru.vkandyba.tm.model.Task;
import ru.vkandyba.tm.model.User;
import ru.vkandyba.tm.util.HashUtil;

import java.sql.Connection;
import java.util.List;

public class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private IPropertyService propertyService;

    public UserService(@NotNull IConnectionService connectionService, @NotNull IPropertyService propertyService) {
        super(connectionService);
        this.propertyService = propertyService;
    }

    @SneakyThrows
    @Nullable
    @Override
    public User findByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            return repository.findByLogin(login);
        } catch (Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    @Nullable
    @Override
    public User findById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyLoginException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            return repository.findById(id);
        } catch (Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeUser(@Nullable User user) {
        if (user == null) throw new EmptyLoginException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.removeByLogin(user.getLogin());
        } catch (Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.removeByLogin(login);
        } catch (Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    @Override
    public void create(@Nullable String login, @Nullable String password, @Nullable String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if(password == null || password.isEmpty()) throw new EmptyPasswordException();
        if(email == null || email.isEmpty()) throw new EmptyNameException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            final User user = new User();
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            user.setEmail(email);
            repository.add(user);
        } catch (Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    @Override
    public void create(@Nullable String login, @Nullable String password, @Nullable Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if(password == null || password.isEmpty()) throw new EmptyPasswordException();
        if(role == null) throw new EmptyNameException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            final User user = new User();
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            user.setRole(role);
            repository.add(user);
        } catch (Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    @Override
    public void create(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if(password == null || password.isEmpty()) throw new EmptyPasswordException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            final User user = new User();
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            repository.add(user);
        } catch (Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    @Override
    public void updateUser(@Nullable String userId, @Nullable String firstName,
                           @Nullable String lastName, @Nullable String middleName)
    {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyIdException();
        if(lastName == null || lastName.isEmpty()) throw new EmptyNameException();
        if(middleName == null || middleName.isEmpty()) throw new EmptyNameException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.updateUser(userId, firstName, lastName, middleName);
            sqlSession.commit();
        } catch (Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public boolean isLoginExists(@Nullable String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @SneakyThrows
    @Override
    public void lockUserByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.lockUserByLogin(login);
            sqlSession.commit();
        } catch (Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    @Override
    public void unlockUserByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.unlockUserByLogin(login);
            sqlSession.commit();
        } catch (Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
