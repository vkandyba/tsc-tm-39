package ru.vkandyba.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.model.Project;
import ru.vkandyba.tm.model.User;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository{

    @NotNull
    @Select("SELECT * FROM app_projects WHERE user_id = #{userId}")
    @Result(column = "id", property = "id")
    @Result(column = "name", property = "name")
    @Result(column = "description", property = "description")
    @Result(column = "date_start", property = "dateStart")
    @Result(column = "date_finish", property = "dateFinish")
    @Result(column = "user_id", property = "userId")
    @Result(column = "status", property = "status")
    List<Project> findAll(@Param("userId") String userId);

    @NotNull
    @Select("SELECT * FROM app_projects WHERE user_id = #{userId} AND name = #{name}")
    @Result(column = "id", property = "id")
    @Result(column = "name", property = "name")
    @Result(column = "description", property = "description")
    @Result(column = "date_start", property = "dateStart")
    @Result(column = "date_finish", property = "dateFinish")
    @Result(column = "user_id", property = "userId")
    @Result(column = "status", property = "status")
    Project findByName(@Param("userId") String userId, @Param("name") String name);

    @Insert("INSERT INTO app_projects(id, name, description, user_id, created_date, date_start, date_finish, status)" +
            "VALUES(#{id}, #{name}, #{description}, #{userId}, #{createdDate}, #{dateStart}, #{dateFinish}, #{status})")
    void add(@NotNull Project project);

    @Delete("DELETE FROM app_projects WHERE user_id = #{userId}  AND name = #{name}")
    void removeByName(@Param("userId") String userId, @Param("name") String name);

    @Update("UPDATE app_projects SET status = 'In progress' WHERE id = #{id} AND user_id = #{userId}")
    void startById(@Param("userId") String userId, @Param("id") String id);

    @Update("UPDATE app_projects SET status = 'In progress' WHERE name = #{name} AND user_id = #{userId}")
    void startByName(@Param("userId") String userId, @Param("name") String name);

    @Update("UPDATE app_projects SET status = 'In progress' WHERE name = #{name} AND user_id = #{userId}")
    void finishById(@Param("userId") String userId, @Param("id") String id);

    @Update("UPDATE app_projects SET status = 'In progress' WHERE name = #{name} AND user_id = #{userId}")
    void finishByName(@Param("userId") String userId, @Param("name") String name);

    @Update("UPDATE app_projects SET name = #{name}, description = #{description}" +
            " WHERE id = #{id} AND user_id = #{userId}")
    void updateById(@Param("userId") String userId, @Param("id") String id, @Param("name") String name, @Param("description") String description);

    @Delete("DELETE FROM app_projects WHERE user_id = #{userId}")
    void clear(@Param("userId") String userId);

}
