package ru.vkandyba.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.repository.IProjectRepository;
import ru.vkandyba.tm.api.repository.ITaskRepository;
import ru.vkandyba.tm.api.service.IConnectionService;
import ru.vkandyba.tm.api.service.IProjectService;
import ru.vkandyba.tm.api.service.IProjectTaskService;
import ru.vkandyba.tm.exception.empty.EmptyIdException;
import ru.vkandyba.tm.model.Project;
import ru.vkandyba.tm.model.Task;

import java.sql.Connection;
import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    IConnectionService connectionService;

    public ProjectTaskService(IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @SneakyThrows
    @NotNull
    @Override
    public List<Task> findAllTaskByProjectId(@Nullable String userId, @Nullable String projectId) {
        if(userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.findAllTaskByProjectId(userId, projectId);
        } catch (Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    @Override
    public void bindTaskToProjectById(@Nullable String userId, @Nullable String projectId, @Nullable String taskId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.bindTaskToProjectById(userId, projectId, taskId);
        } catch (Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    @Override
    public void unbindTaskToProjectById(@Nullable String userId, @Nullable String projectId, @Nullable String taskId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.unbindTaskToProjectById(userId, projectId, taskId);
        } catch (Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeAllTaskByProjectId(@Nullable String userId, @Nullable String projectId) {
        if(userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.removeAllTaskByProjectId(userId, projectId);
        } catch (Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeById(@Nullable String userId, @Nullable String projectId) {
        if(userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.removeById(userId, projectId);
        } catch (Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
