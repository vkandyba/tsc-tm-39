package ru.vkandyba.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.model.Project;
import ru.vkandyba.tm.model.Session;
import ru.vkandyba.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IProjectEndpoint {

    @Nullable
    @WebMethod
    List<Project> findAllProjects(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    );

    @WebMethod
    void addProject(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "project", partName = "project") @NotNull Project project
    );


    @WebMethod
    void finishProjectById(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull String projectId
    );

    @WebMethod
    void finishProjectByName(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    );

    @WebMethod
    void removeProjectById(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull String projectId
    );

    @WebMethod
    void removeProjectByName(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    );

    @WebMethod
    Project showProjectById(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull String projectId
    );

    @WebMethod
    void startProjectById(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull String projectId
    );


    @WebMethod
    void startProjectByName(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    );

    @WebMethod
    void updateProjectById(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull String projectId,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "description", partName = "description") @NotNull String description

    );

    @WebMethod
    void removeProjectWithTasksById(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull String projectId
    );

    @WebMethod
    void clearProjects(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    );
}
