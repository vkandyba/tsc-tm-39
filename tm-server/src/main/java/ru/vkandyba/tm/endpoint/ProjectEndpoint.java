package ru.vkandyba.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.endpoint.IProjectEndpoint;
import ru.vkandyba.tm.api.service.ServiceLocator;
import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.model.Project;
import ru.vkandyba.tm.model.Session;

import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Nullable
    @Override
    public List<Project> findAllProjects(@NotNull Session session) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAll(session.getUserId());
    }

    @Override
    public void addProject(@NotNull Session session, @NotNull Project project) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().add(session.getUserId(),project);
    }

    @Override
    public void finishProjectById(@NotNull Session session, @NotNull String projectId) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().finishById(session.getUserId(), projectId);
    }

    @Override
    public void finishProjectByName(@NotNull Session session, @NotNull String name) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().finishByName(session.getUserId(), name);
    }

    @Override
    public void removeProjectById(@NotNull Session session, @NotNull String projectId) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().removeById(session.getUserId(), projectId);
    }

    @Override
    public void removeProjectByName(@NotNull Session session, @NotNull String name) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().removeByName(session.getUserId(), name);
    }

    @Override
    public Project showProjectById(@NotNull Session session, @NotNull String projectId) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findById(session.getUserId(), projectId);
    }

    @Override
    public void startProjectById(@NotNull Session session, @NotNull String projectId) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().startById(session.getUserId(), projectId);
    }

    @Override
    public void startProjectByName(@NotNull Session session, @NotNull String name) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().startByName(session.getUserId(), name);
    }

    @Override
    public void updateProjectById(@NotNull Session session, @NotNull String projectId, @NotNull String name, @NotNull String description) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().updateById(session.getUserId(), projectId, name, description);
    }

    @Override
    public void removeProjectWithTasksById(@NotNull Session session, @NotNull String projectId) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().removeById(session.getUserId(), projectId);
    }

    @Override
    public void clearProjects(@NotNull Session session) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().clear(session.getUserId());
    }

}
