package ru.vkandyba.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    String getDeveloperName();

    @NotNull
    String getDeveloperEmail();

    @NotNull
    String getVersion();

    @NotNull
    String getHashSecret();

    @NotNull
    String getHashIteration();

    @NotNull
    String getServerHost();

    @NotNull
    String getServerPort();

}
