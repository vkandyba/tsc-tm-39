package ru.vkandyba.tm.api.service;

import ru.vkandyba.tm.api.repository.IRepository;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.model.Project;
import ru.vkandyba.tm.model.User;

import java.util.List;

public interface IUserService{

    User findByLogin(String login);

    User findById(String id);

    void removeUser(User user);

    void removeByLogin(String login);

    void create(String login, String password, String email);

    void create(String login, String password, Role role);

    void create(String login, String password);

    void updateUser(String userId, String firstName, String lastName, String middleName);

    boolean isLoginExists(String login);

    void lockUserByLogin(String login);

    void unlockUserByLogin(String login);

}
