package ru.vkandyba.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.repository.IProjectRepository;
import ru.vkandyba.tm.api.repository.IRepository;
import ru.vkandyba.tm.api.service.IConnectionService;
import ru.vkandyba.tm.api.service.IService;
import ru.vkandyba.tm.exception.empty.EmptyIdException;
import ru.vkandyba.tm.exception.empty.EmptyIndexException;
import ru.vkandyba.tm.model.AbstractEntity;

import java.sql.Connection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<E extends AbstractEntity>{

    @NotNull
    IConnectionService connectionService;

    public AbstractService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
