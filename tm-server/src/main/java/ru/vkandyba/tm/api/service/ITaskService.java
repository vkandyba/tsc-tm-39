package ru.vkandyba.tm.api.service;

import ru.vkandyba.tm.api.repository.IBusinessRepository;
import ru.vkandyba.tm.api.repository.IRepository;
import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.model.Project;
import ru.vkandyba.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService{

    List<Task> findAll(String userId);

    Task findByName(String userId, String name);

    Task findById(String userId, String id);

    void add(String userId, Task task);

    void removeByName(String userId, String name);

    void removeById(String userId, String id);

    void updateById(String userId, String id, String name, String description);

    void startById(String userId, String id);

    void startByName(String userId, String name);

    void finishById(String userId, String id);

    void finishByName(String userId, String name);

    void clear(String userId);

}
