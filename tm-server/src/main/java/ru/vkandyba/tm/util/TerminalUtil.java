package ru.vkandyba.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Scanner;

public interface TerminalUtil {

    @NotNull Scanner SCANNER = new Scanner(System.in);

    @Nullable
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @Nullable
    static Integer nextNumber() {
        final String value = SCANNER.nextLine();
        return Integer.parseInt(value);
    }

}

