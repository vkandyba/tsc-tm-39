package ru.vkandyba.tm.api.repository;

import org.apache.ibatis.annotations.Insert;
import ru.vkandyba.tm.model.Session;

public interface ISessionRepository extends IRepository<Session> {

    @Insert("INSERT INTO app_sessions(id, user_id, signature, timestamp) " +
            "VALUES(#{id}, #{userId}, #{signature}, #{timestamp})")
    void add(Session session);

}
