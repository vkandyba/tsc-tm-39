package ru.vkandyba.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.model.User;

import java.util.List;

public interface IUserRepository{

    @Insert("INSERT INTO app_users(id, login, locked, password, " +
            "email, first_name, last_name, middle_name, role) " +
            "VALUES(#{id}, #{login}, #{lock}, #{passwordHash}, " +
            "#{email}, #{firstName}, #{lastName}, #{middleName}, #{role})")
    void add(@NotNull User user);

    @Delete("DELETE FROM app_users")
    void clear();

    @Select("SELECT * FROM app_users WHERE login = #{login}")
    @Result(column = "id", property = "id")
    @Result(column = "login", property = "login")
    @Result(column = "password", property = "passwordHash")
    @Result(column = "email", property = "email")
    @Result(column = "first_name", property = "firstName")
    @Result(column = "last_name", property = "lastName")
    @Result(column = "middle_name", property = "middleName")
    @Result(column = "role", property = "role")
    @Result(column = "locked", property = "locked")
    User findByLogin(@Param("login") @NotNull String login);

    @Select("SELECT * FROM app_users WHERE id = #{id}")
    @Result(column = "id", property = "id")
    @Result(column = "login", property = "login")
    @Result(column = "password", property = "passwordHash")
    @Result(column = "email", property = "email")
    @Result(column = "first_name", property = "firstName")
    @Result(column = "last_name", property = "lastName")
    @Result(column = "middle_name", property = "middleName")
    @Result(column = "role", property = "role")
    @Result(column = "locked", property = "locked")
    User findById(@Param("id") @NotNull String id);

    @Delete("DELETE FROM app_users WHERE login = #{login}")
    void removeByLogin(@Param("login") @NotNull String login);

    @Update("UPDATE app_users SET first_name = #{firstName}, last_name = #{lastName}, " +
            "middle_name = #{middleName} WHERE id = #{id}")
    void updateUser(@NotNull String userId, @NotNull String firstName, @NotNull String lastName, @NotNull String middleName);


    @Update("UPDATE app_users SET locked = 1 WHERE id = #{id}")
    void lockUserByLogin(@NotNull String login);

    @Update("UPDATE app_users SET locked = 0 WHERE id = #{id}")
    void unlockUserByLogin(@NotNull String login);

}
