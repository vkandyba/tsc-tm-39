package ru.vkandyba.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.endpoint.IAdminEndpoint;
import ru.vkandyba.tm.api.service.ServiceLocator;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.model.Session;
import ru.vkandyba.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    public AdminEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public void createUser(@NotNull String login, @NotNull String password, @NotNull String email) {
        serviceLocator.getUserService().create(login, password, email);
    }

    @Override
    public void removeUser(@NotNull Session session, @NotNull String login) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().removeUser(serviceLocator.getUserService().findByLogin(login));
    }

    @Override
    public void lockUser(@NotNull Session session,@NotNull String login) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().lockUserByLogin(login);
    }

    @Override
    public void unLockUser(@NotNull Session session,@NotNull String login) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().unlockUserByLogin(login);
    }
}
