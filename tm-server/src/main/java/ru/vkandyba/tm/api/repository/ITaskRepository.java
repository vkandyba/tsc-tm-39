package ru.vkandyba.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.model.Project;
import ru.vkandyba.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IBusinessRepository<Task> {

    @NotNull
    @Select("SELECT * FROM app_tasks WHERE user_id = #{userId}")
    @Result(column = "id", property = "id")
    @Result(column = "name", property = "name")
    @Result(column = "description", property = "description")
    @Result(column = "start_date", property = "startDate")
    @Result(column = "finish_date", property = "finishDate")
    @Result(column = "user_id", property = "userId")
    @Result(column = "status", property = "status")
    @Result(column = "project_id", property = "projectId")
    List<Task> findAll(@Param("userId") String userId);

    @NotNull
    @Select("SELECT * FROM app_tasks WHERE user_id = #{userId} AND name = #{name}")
    @Result(column = "id", property = "id")
    @Result(column = "name", property = "name")
    @Result(column = "description", property = "description")
    @Result(column = "start_date", property = "startDate")
    @Result(column = "finish_date", property = "finishDate")
    @Result(column = "user_id", property = "userId")
    @Result(column = "status", property = "status")
    @Result(column = "project_id", property = "projectId")
    Task findByName(@Param("userId") String userId, @Param("name") String name);

    @Insert("INSERT INTO app_tasks(id, name, description, user_id, created_date, start_date, finish_date, status, project_id)" +
            "VALUES(#{id}, #{name}, #{description}, #{userId}, #{createdDate}, #{startDate}, #{finishDate}, #{status}, #{projectId})")
    void add(@NotNull Task task);

    @Delete("DELETE FROM app_tasks WHERE user_id = #{userId}  AND name = #{name}")
    void removeByName(@Param("userId") String userId, @Param("name") String name);

    @Update("UPDATE app_tasks SET status = 'In progress' WHERE id = #{id} AND user_id = #{userId}")
    void startById(@Param("userId") String userId, @Param("id") String id);

    @Update("UPDATE app_tasks SET status = 'In progress' WHERE name = #{name} AND user_id = #{userId}")
    void startByName(@Param("userId") String userId, @Param("name") String name);

    @Update("UPDATE app_tasks SET status = 'In progress' WHERE name = #{name} AND user_id = #{userId}")
    void finishById(@Param("userId") String userId, @Param("id") String id);

    @Update("UPDATE app_tasks SET status = 'In progress' WHERE name = #{name} AND user_id = #{userId}")
    void finishByName(@Param("userId") String userId, @Param("name") String name);

    @Update("UPDATE app_tasks SET name = #{name}, description = #{description}" +
            " WHERE id = #{id} AND user_id = #{userId}")
    void updateById(@Param("userId") String userId, @Param("id") String id, @Param("name") String name, @Param("description") String description);

    @Delete("DELETE FROM app_tasks WHERE user_id = #{userId}")
    void clear(@Param("userId") String userId);

    @Update("UPDATE app_tasks SET project_id = #{projectId} WHERE id = #{id} AND user_id = #{userId}")
    void bindTaskToProjectById(@Param("userId") String userId, @Param("projectId") String projectId, @Param("id") String taskId);

    @Update("UPDATE app_tasks SET project_id = NULL WHERE id = #{id} AND user_id = #{userId}")
    void unbindTaskToProjectById(@Param("userId") String userId, String projectId, @Param("id") String taskId);

    @Select("SELECT * FROM app_tasks WHERE user_id = #{userId} AND project_id = #{projectId}")
    @Result(column = "id", property = "id")
    @Result(column = "name", property = "name")
    @Result(column = "description", property = "description")
    @Result(column = "start_date", property = "startDate")
    @Result(column = "finish_date", property = "finishDate")
    @Result(column = "user_id", property = "userId")
    @Result(column = "status", property = "status")
    @Result(column = "project_id", property = "projectId")
    List<Task> findAllTaskByProjectId(@Param("userId") String userId, @Param("projectId") String projectId);

    @Delete("DELETE FROM app_tasks WHERE user_id = #{userId} AND project_id = #{projectId}")
    void removeAllTaskByProjectId(@Param("userId") String userId, @Param("projectId") String projectId);

}
