package ru.vkandyba.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.endpoint.Session;
import ru.vkandyba.tm.endpoint.Task;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.exception.entity.TaskNotFoundException;
import ru.vkandyba.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskStartByNameCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "task-start-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Start task by name...";
    }

    @Override
    public void execute() {
        @NotNull final Session session = serviceLocator.getSession();
        System.out.println("Enter name");
        @Nullable final String name = TerminalUtil.nextLine();
        serviceLocator.getTaskEndpoint().startTaskByName(session, name);
    }

    @NotNull
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
