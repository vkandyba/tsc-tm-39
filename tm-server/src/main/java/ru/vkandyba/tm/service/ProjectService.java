package ru.vkandyba.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.repository.IProjectRepository;
import ru.vkandyba.tm.api.service.IConnectionService;
import ru.vkandyba.tm.api.service.IProjectService;
import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.exception.empty.EmptyIdException;
import ru.vkandyba.tm.exception.empty.EmptyIndexException;
import ru.vkandyba.tm.exception.empty.EmptyNameException;
import ru.vkandyba.tm.model.Project;

import java.util.List;

public class ProjectService extends AbstractService<Project> implements IProjectService {

    public ProjectService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    public List<Project> findAll(String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.findAll(userId);
        } catch (Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    @Nullable
    @Override
    public Project findByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.findByName(userId, name);
        } catch (Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public Project findById(String userId, String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyNameException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.findByName(userId, id);
        } catch (Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void add(String userId, Project project) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            project.setUserId(userId);
            projectRepository.add(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.removeByName(userId, name);
            sqlSession.commit();
        } catch (Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeById(String userId, String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyNameException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.removeByName(userId, id);
            sqlSession.commit();
        } catch (Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    @Override
    public void updateById(String userId, String id, String name, String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if(name == null || name.isEmpty()) throw new EmptyNameException();
        if(description == null || description.isEmpty()) throw new EmptyNameException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.updateById(userId, id, name, description);
            sqlSession.commit();
        } catch (Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    @Override
    public void startById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.startById(userId, id);
            sqlSession.commit();
        } catch (Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    @Override
    public void startByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.startByName(userId, name);
            sqlSession.commit();
        } catch (Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    @Override
    public void finishById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.finishById(userId, id);
            sqlSession.commit();
        } catch (Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    @Override
    public void finishByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.finishByName(userId, name);
            sqlSession.commit();
        } catch (Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void clear(String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.clear(userId);
            sqlSession.commit();
        } catch (Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
