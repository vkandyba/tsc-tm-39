package ru.vkandyba.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.repository.IRepository;
import ru.vkandyba.tm.api.repository.ISessionRepository;
import ru.vkandyba.tm.api.service.*;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.exception.system.AccessDeniedException;
import ru.vkandyba.tm.model.Session;
import ru.vkandyba.tm.model.User;
import ru.vkandyba.tm.util.HashUtil;

import java.sql.Connection;
import java.util.Optional;

public class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    final IUserService userService;

    public SessionService(@NotNull IConnectionService connectionService, @NotNull IUserService userService) {
        super(connectionService);
        this.userService = userService;
    }

    @SneakyThrows
    @Nullable
    @Override
    public Session open(
            @Nullable final String login,
            @Nullable final String password
    ) {
        final boolean check = checkDataAccess(login, password);
        if (!check) return null;
        final User user = userService.findByLogin(login);
        if (user == null) return null;
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            repository.add(session);
            sqlSession.commit();
            return sign(session);
        } catch (Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }


    @SneakyThrows
    @Override
    public void close(@Nullable final Session session) {
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            repository.removeById(session.getId());
        } catch (Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public boolean checkDataAccess(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login.isEmpty() || password.isEmpty()) return false;
        final User user = userService.findByLogin(login);
        if (user == null) return false;
        final String passwordHash = HashUtil.salt(connectionService.getPropertyService(), password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @SneakyThrows
    @Override
    public void validate(Session session) {
        if(session == null) throw new AccessDeniedException();
        if(session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if(session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessDeniedException();
        if(session.getTimestamp() == null) throw new AccessDeniedException();
        @NotNull final Session temp = session.clone();
        @NotNull final String signatureSource = session.getSignature();
        @NotNull final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if(!check) throw new AccessDeniedException();
    }

    @Override
    public void validate(Session session, Role role) {
        validate(session);
        @Nullable final User user = userService.findById(session.getUserId());
        if(user == null) throw new AccessDeniedException();
        if(!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    public @Nullable Session sign(Session session) {
        if(session == null) return null;
        session.setSignature(null);
        @Nullable final String signature = HashUtil.sign(connectionService.getPropertyService(), session);
        session.setSignature(signature);
        return session;
    }

}
