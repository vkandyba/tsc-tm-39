package ru.vkandyba.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.model.Session;
import ru.vkandyba.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IAdminEndpoint {

    @WebMethod
    void createUser(
            @WebParam(name = "login", partName = "login") @NotNull String login,
            @WebParam(name = "password", partName = "password") @NotNull String password,
            @WebParam(name = "email", partName = "email") @NotNull String email
    );

    @WebMethod
    void removeUser(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "login", partName = "login") @NotNull String login
    );

    @WebMethod
    void lockUser(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "login", partName = "login") @NotNull String login
    );

    @WebMethod
    void unLockUser(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "login", partName = "login") @NotNull String login
    );
}
