package ru.vkandyba.tm.exception.empty;

import ru.vkandyba.tm.exception.AbstractException;

public class EmptyLoginException extends AbstractException {

    public EmptyLoginException() {
        super("Error! Login is empty!");
    }

}
