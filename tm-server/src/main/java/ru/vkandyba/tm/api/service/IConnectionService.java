package ru.vkandyba.tm.api.service;

import org.apache.ibatis.session.SqlSession;

import java.sql.Connection;

public interface IConnectionService {

    IPropertyService getPropertyService();

    SqlSession getSqlSession();

}
